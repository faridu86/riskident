**Fight the bad guys**

First of all Nodejs Should be installed on your machine to the application, for the rest you should follow the given steps.

1. cd in directory.
2. run 'npm install'
3. run 'npm run start' app will start on port 3000, but make sure no other is running on 3000 or change port in index.js file.
4. open any browser and open url 'http://localhost:3000/api/transactions?transactionId={transactionId}&confidenceLevel={confidenceLevel}'
5. replace transactionId and confidenceLevel according the tests you need.


If you want to check on live machine, please check on ip below.

http://52.73.49.108/api/transactions?transactionId=5c868b2213b36f773efcee81&confidenceLevel=1