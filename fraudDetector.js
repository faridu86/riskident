const testData = require('./test-data');

const getTransactions = (transactionId, confidenceLevel) => {
  let transaction = null;
  const matchedTransactions = [];

  const findTransaction = (transactions) => {
    if (transaction !== null) {
      return;
    }

    for (let i = 0; i < transactions.length; i += 1) {
      const t = transactions[i];
      if (transactionId === t.id) {
        transaction = t;
        return;
      }

      if (t.children) findTransaction(t.children);
    }
  };

  const extractChildTransaction = (obj, parent = null) => {
    const {
      id, index, age, name, email, phone, connectionInfo, children,
    } = obj;
    const formatObj = {
      id, index, age, name, email, phone,
    };

    if (transactionId !== obj.id && connectionInfo) {
      formatObj.connectionInfo = connectionInfo;
    }

    formatObj.combinedConnectionInfo = {
      types: (!parent && obj.connectionInfo && obj.connectionInfo.type && [obj.connectionInfo.type])
        || (parent && parent.combinedConnectionInfo && parent.combinedConnectionInfo.types && obj.connectionInfo && obj.connectionInfo.type
          && [...parent.combinedConnectionInfo.types, obj.connectionInfo && obj.connectionInfo.type])
        || (obj.connectionInfo && obj.connectionInfo.type && [obj.connectionInfo.type])
        || [],
      // confidence is calculated like we are doing it for types, by multiplying all parent values.
      confidence: (!parent && 1)
        || (parent && parent.combinedConnectionInfo && parent.combinedConnectionInfo.confidence
          && parent.combinedConnectionInfo.confidence * obj.connectionInfo.confidence)
        || obj.connectionInfo.confidence,
    };

    // eslint-disable-next-line no-irregular-whitespace
    // along with all its children that have a connection confidence same or bigger than the ​confidenceLevel​ query param.
    if (parent === null || obj.connectionInfo.confidence >= parseFloat(confidenceLevel)) {
      matchedTransactions.push(formatObj);
    }

    if (children) {
      for (let i = 0; i < children.length; i += 1) {
        const c = children[i];
        extractChildTransaction(c, { ...obj, combinedConnectionInfo: formatObj.combinedConnectionInfo });
      }
    }
  };

  if (testData) findTransaction(testData);

  if (transaction) {
    extractChildTransaction(transaction);
  }

  return matchedTransactions;
};

module.exports = {
  getTransactions,
};
