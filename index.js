const express = require('express');
const app = express();

const fraudDetector = require('./fraudDetector');

const port = 3000;

app.get('/api/transactions', async (req, res) => {
  const { transactionId, confidenceLevel } = req.query;
  if(!transactionId || !confidenceLevel) {
    res.send('Send transactionId and confidenceLevel in query params');
    return;
  }
  const transactions = await fraudDetector.getTransactions(transactionId, confidenceLevel);
  res.json(transactions);
})

app.listen(port, () => console.log(`App listening on port ${port}!`)); 
